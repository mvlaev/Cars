# Cars

[Django REST API](https://www.django-rest-framework.org/) for managing User, UserCar, CarModel and CarBrand models.

### General Info:

The UserCar model is related to the User, CarModel and CarBrand models.
The CarModel model is related to CarBrand.
All models are implemented with cascade soft delete thanks to the [django-safedelete library](https://django-safedelete.readthedocs.io/en/latest/index.html).

All models are accessible through the Django's admin site.

The project is configured to use SQLite database but there is **NO** database file provided. Make sure to make and run migrations.

# Dependencies

The project depends directly on [Django](https://www.djangoproject.com/), [Django REST Framework](https://www.django-rest-framework.org/) and [django-safedelete](https://django-safedelete.readthedocs.io/en/latest/index.html).

All dependencies are in the requirements.txt file.
****

# API Docs

Access points:

* `admin/` - Django's Admin Site

* `api/users/` - List `User`s.
* `api/users/create/` - Create new `User`.
* `api/users/<str:username>/` - Detail/GeneralUpdate/Delete `User`s by 'username'.
* `api/users/<str:username>/passupdate/` - View/Update/Delete `User`s by 'username'.

* `api/brands/` - List/Create `CarBrand`s.
* `api/models/` - List/Create `CarModel`s.
* `api/cars/` - List/Create `UserCar`s.
* `api/cars/<int:pk>/` - View/Update/Delete `UserCar`s by 'pk'.
****

# Development Docs

It's a Django project that uses Django REST Framework.
The project consists of two apps: `users_app` and `cars_app`

### users_app

**Models:**

* __users_app.models.User__
	This model is a customized User model. It inherits both Django's `AbstractUser` and django-safedelete's `SafeDeleteModel`.

	The User model has its own custom Manager which is called `UserManagerWithSoftDelete` that inherits both Django's `UserManager` and django-safedelete's `SafeDeleteManager`. This way the User has all the functionality of the default Django User, including password hashing etc., and also has the soft delete capabilities provided by `SafeDeleteManager`.

	The User model is listed as default AUTH_USER_MODEL in the project's settings and is entirely exposed in Django's admin site, including the custom fields and the soft deleted status.

	* Fields:
		* all default Django User fields
	* Custom fields:
		*  **email** - obligatory, unique, EmailField
		*  **has_driver_license** - optional, BooleanField, default=False
		*  **driver_license_exp_date** - optional, DateField

**Serializers:**

All serializers use Django REST Framework's `ModelSerializer` class from the `serializers` module.

* **users_app.serializers.UserCreateSerializer**
    * `fields = ['username', 'password', 'email', 'first_name', 'last_name', 'has_driver_license', 'driver_license_exp_date']`
* **users_app.serializers.UserListSerializer**
    * `fields = ['id', 'username', 'first_name', 'last_name', 'email', 'has_driver_license', 'driver_license_exp_date', 'deleted']`
* **users_app.serializers.UserGeneralUpdateSerializer**
    * `fields = ['first_name', 'last_name', 'email', 'has_driver_license', 'driver_license_exp_date']`
* **users_app.serializers.UserPassUpdateSerializer**
    * `fields = ['username', 'password']`
    * `read_only_fields = ['username']`

**Views:**

All views are implemented as Class based views that inherit from the classes of the Django REST Framework's `generics` module.

All views, ***EXCEPT*** for the `UserCreate` one, have default Django permissions provided via the Django REST Framework's `permissions.DjangoModelPermissions` class.

* **users_app.views.UserCreate**
    * Creates new users with hashed password.
    * ***NOTE:*** ***ANYONE can create account, NO VERIFICATION or AUTHENTICATION ADDED!***
* **users_app.views.UserList**
    * Lists all users, ***including*** the soft-deleted ones.
* **users_app.views.UserDetail**
    * Detailed user view.
    * Enables deletion and basic modification of users.
    * Does not allow soft deleted users.
* **users_app.views.UserPassUpdate**
    * Enables password change. The new password is hashed using the hasher specified in the project settings.

**URLs:**

All urls can be used with format suffixes thanks to the Django REST Framework's urlpatterns module.

* `users/` - List `User`s.
* `users/create/` - Create new `User`.
* `users/<str:username>/` - Detail/GeneralUpdate/Delete `User`s by 'username'.
* `users/<str:username>/passupdate/` - View/Update/Delete `User`s by 'username'.
***

### cars_app

**Models:**

All models inherit from django-safedelete's `SafeDeleteModel` class to provide soft delete functionality.

*   **cars_app.models.CarBrand**
    * Fields:
        * **name** - obligatory, CharField, unique
	    * **created_at** - auto_now_add, DateTimeField
	    * **deleted_at** - automatic, DateTimeField
*   **cars_app.models.CarModel**
    * Fields
        * **name** - obligatory, unique, CharField
        * **car_brand** - ForeignKey to `CarBrand`, pointing at the `'pk'`
        * **created_at** - auto_now_add, DateTimeField
        * **update_at** - auto_now, DateTimeField
*   **cars_app.models.UserCar**
	* Fields:
		* **user** - ForeignKey to `'users_app.User'`, pointing at `'username'`
		* **car_brand** - ForeignKey to `'CarBrand'`, pointing at `'pk'`
		* **car_model** - ForeignKey to `'CarModel'`, pointing at `'pk'`
		* **first_reg** - obligatory, DateField
		* **odometer** - optional, IntegerField, default=0
		* **created_at** - auto_now_add, DateTimeField
		* **deleted_at** - automatic, DateTimeField

**Serializers:**

All serializers use Django REST Framework's `ModelSerializer` class from the `serializers` module.

* **cars_app.serializers.CarBrandSerializer**
    * `fields = ['id', 'name', 'created_at', 'deleted_at']`
*  **cars_app.serializers.CarModelSerializer**
    * `fields = ['id', 'car_brand', 'name', 'created_at', 'update_at']`
*  **cars_app.serializers.UserCarSerializer**
    *  `fields = ['id', 'user', 'car_brand', 'car_model', 'first_reg', 'odometer']`
*  **cars_app.serializers.UserCarDetailSerializer**
    *  `fields = ['id', 'user', 'car_brand', 'car_model', 'first_reg', 'odometer', 'created_at', 'deleted_at']`

**Views:**

All views are implemented as Class based views that inherit from the classes of the Django REST Framework's `generics` module.

All views have default Django permissions provided via the Django REST Framework's `permissions.DjangoModelPermissions` class.

* **cars_app.views.CarBrandList**
    * Enables CarBrand list and create.
* **cars_app.views.CarModelList**
    * Enables CarModel list and create
* **cars_app.views.UserCarList**
    * Enables UserCar list and create
* **cars_app.views.UserCarDetail**
    * Enables UserCar detailed view, update and delete

**URLs:**

All urls can be used with format suffixes thanks to the Django REST Framework's urlpatterns module.

* `brands/` - List/Create `CarBrand`s.
* `models/` - List/Create `CarModel`s.
* `cars/` - List/Create `UserCar`s.
* `cars/<int:pk>/` - View/Update/Delete `UserCar`s by 'pk'.
****
