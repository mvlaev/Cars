asgiref==3.5.0
Django==4.0.4
django-safedelete==1.1.2
djangorestframework==3.13.1
pytz==2022.1
sqlparse==0.4.2
