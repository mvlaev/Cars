from django.shortcuts import render

# Create your views here.
# REST API docs:
# https://www.django-rest-framework.org/api-guide/generic-views/
from rest_framework import generics

# Add permissions
# https://www.django-rest-framework.org/tutorial/4-authentication-and-permissions/#adding-required-permissions-to-views
from rest_framework import permissions

from .models import CarBrand, CarModel, UserCar
from .serializers import CarBrandSerializer, CarModelSerializer, UserCarSerializer, UserCarDetailSerializer

# CarBrand CRUD
class CarBrandList(generics.ListCreateAPIView):
	queryset = CarBrand.objects.all()
	serializer_class = CarBrandSerializer
	permission_classes = [permissions.DjangoModelPermissions]

# CarModel CRUD
class CarModelList(generics.ListCreateAPIView):
	queryset = CarModel.objects.all()
	serializer_class = CarModelSerializer
	permission_classes = [permissions.DjangoModelPermissions]

# UserCar CRUD
class UserCarList(generics.ListCreateAPIView):
	queryset = UserCar.objects.all()
	serializer_class = UserCarSerializer
	permission_classes = [permissions.DjangoModelPermissions]

class UserCarDetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = UserCar.objects.all()
	serializer_class = UserCarDetailSerializer
	permission_classes = [permissions.DjangoModelPermissions]
