from django.contrib import admin

# Register your models here.
from . import models

from django.contrib import admin
from safedelete.admin import SafeDeleteAdmin

class CarsSafeDeleteAdmin(SafeDeleteAdmin, admin.ModelAdmin):
	list_display = admin.ModelAdmin.list_display + SafeDeleteAdmin.list_display
	#fieldsets = list(admin.ModelAdmin.fieldsets) + [('Additional info', {'fields':('created_at', 'deleted_at')})]

class CarModelSafeDeleteAdmin(CarsSafeDeleteAdmin):
	list_display = admin.ModelAdmin.list_display + ('car_brand', ) + SafeDeleteAdmin.list_display

class UserCarSafeDeleteAdmin(CarsSafeDeleteAdmin):
	list_display = admin.ModelAdmin.list_display + ('user', 'car_model', 'car_brand') + SafeDeleteAdmin.list_display

admin.site.register(models.CarBrand, CarsSafeDeleteAdmin)
admin.site.register(models.CarModel, CarModelSafeDeleteAdmin)
admin.site.register(models.UserCar, UserCarSafeDeleteAdmin)
