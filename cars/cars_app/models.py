from django.db import models

# Using django-safedelete for soft deleting
# https://django-safedelete.readthedocs.io/en/latest/index.html

from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE


# Create your models here.
# Fields reference here:
# https://docs.djangoproject.com/en/4.0/ref/models/fields/#module-django.db.models.fields

class CarBrand(SafeDeleteModel):
	_safedelete_policy = SOFT_DELETE_CASCADE

	name       = models.CharField(max_length=100, unique=True)
	created_at = models.DateTimeField(auto_now_add=True)
	deleted_at = SafeDeleteModel.deleted

	def __str__(self):
		return self.name

class CarModel(SafeDeleteModel):
	_safedelete_policy = SOFT_DELETE_CASCADE

	car_brand  = models.ForeignKey('CarBrand', on_delete=models.CASCADE)
	name       = models.CharField(max_length=100, unique=True)
	created_at = models.DateTimeField(auto_now_add=True)
	update_at  = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

class UserCar(SafeDeleteModel):
	_safedelete_policy = SOFT_DELETE_CASCADE

	user       = models.ForeignKey('users_app.User', to_field='username', on_delete=models.CASCADE)
	car_brand  = models.ForeignKey('CarBrand',       on_delete=models.CASCADE)
	car_model  = models.ForeignKey('CarModel',       on_delete=models.CASCADE)
	first_reg  = models.DateField()
	odometer   = models.IntegerField(default=0)
	created_at = models.DateTimeField(auto_now_add=True)
	deleted_at = SafeDeleteModel.deleted

	def __str__(self):
		return f'{self.user.username}, {self.car_brand.name}, {self.car_model.name}'
