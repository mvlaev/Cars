# cars_app urls

from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('brands/', views.CarBrandList.as_view()),
    path('models/', views.CarModelList.as_view()),
    path('cars/', views.UserCarList.as_view()),
    path('cars/<int:pk>/', views.UserCarDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
