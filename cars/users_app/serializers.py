# Serializers for the users_app models
from rest_framework import serializers
from .models import User

class UserCreateSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ['username', 'password', 'email', 'first_name', 'last_name', 'has_driver_license', 'driver_license_exp_date']

class UserListSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ['id', 'username', 'first_name', 'last_name', 'email', 'has_driver_license', 'driver_license_exp_date', 'deleted']

class UserGeneralUpdateSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ['first_name', 'last_name', 'email', 'has_driver_license', 'driver_license_exp_date']

class UserPassUpdateSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ['username', 'password']
		read_only_fields = ['username']
