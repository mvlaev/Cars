# Create your views here.
# REST API docs:
# https://www.django-rest-framework.org/api-guide/generic-views/
from rest_framework import generics

# REST API Permissions
# https://www.django-rest-framework.org/tutorial/4-authentication-and-permissions/#adding-required-permissions-to-views
from rest_framework import permissions

from .models import User
from .serializers import UserCreateSerializer, UserListSerializer, UserGeneralUpdateSerializer, UserPassUpdateSerializer

# User CRUD
class UserList(generics.ListAPIView):
	queryset = User.objects.all_with_deleted()
	serializer_class = UserListSerializer
	permission_classes = [permissions.DjangoModelPermissions]

class UserCreate(generics.CreateAPIView):
	serializer_class = UserCreateSerializer

	def perform_create(self, serializer):
		User.objects.create_user(**serializer.validated_data)

class UserDetail(generics.RetrieveUpdateDestroyAPIView):
	'''Retrieve/Update/Delete
	'''
	queryset = User.objects.all()
	lookup_field = 'username'
	permission_classes = [permissions.DjangoModelPermissions]

	def get_serializer_class(self):
		if self.request.method == 'GET':
			return UserListSerializer
		return UserGeneralUpdateSerializer

class UserPassUpdate(generics.RetrieveUpdateAPIView):
	queryset = User.objects.all()
	lookup_field = 'username'
	permission_classes = [permissions.DjangoModelPermissions]

	def get_serializer_class(self):
		if self.request.method == 'GET':
			return UserListSerializer
		return UserPassUpdateSerializer

	def perform_update(self, serializer):
		user = self.get_object()
		user.set_password(serializer.validated_data['password'])
		user.save()
