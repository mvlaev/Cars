from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager

# Using django-safedelete for soft deleting
# https://django-safedelete.readthedocs.io/en/latest/index.html

from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE
from safedelete.managers import SafeDeleteManager

# Own user manager
# SafeDeleteManager CAN NOT create_user() so hashing does not work
# https://docs.djangoproject.com/en/4.0/topics/auth/customizing/#writing-a-manager-for-a-custom-user-model

class UserManagerWithSoftDelete(SafeDeleteManager, UserManager):
	pass

# User model extended from AbstractUser (see link) and SafeDeleteModel
# https://docs.djangoproject.com/en/4.0/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project
# https://django-safedelete.readthedocs.io/en/latest/index.html

class User(SafeDeleteModel, AbstractUser):
	email = models.EmailField(unique=True, null=False, blank=False)
	has_driver_license = models.BooleanField(blank=True, default=False)
	driver_license_exp_date = models.DateField(blank=True, null=True)

	objects = UserManagerWithSoftDelete()

	def __str__(self):
		return self.username

