from django.contrib import admin

# Register your models here.
# https://docs.djangoproject.com/en/4.0/ref/contrib/admin/#modeladmin-objects
from .models import User
from django.contrib.auth.admin import UserAdmin

# SafeDeleteAdmin docs:
# https://django-safedelete.readthedocs.io/en/latest/admin.html
from safedelete.admin import SafeDeleteAdmin

class UserSafeDeleteAdmin(SafeDeleteAdmin, UserAdmin):
	list_display = UserAdmin.list_display + SafeDeleteAdmin.list_display
	fieldsets = list(UserAdmin.fieldsets) + [('Additional info', {'fields':('has_driver_license', 'driver_license_exp_date')})]

admin.site.register(User, UserSafeDeleteAdmin)
