# users_app urls

from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('users/', views.UserList.as_view()),
    path('users/create/', views.UserCreate.as_view()),
    path('users/<str:username>/', views.UserDetail.as_view()),
	path('users/<str:username>/passupdate/', views.UserPassUpdate.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
